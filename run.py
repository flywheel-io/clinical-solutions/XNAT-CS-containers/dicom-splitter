#!/usr/bin/env python
"""The run script"""
import os
import logging
import sys
import traceback
from pathlib import Path
import xnat

from fw_gear_dicom_splitter.main import run
from fw_gear_dicom_splitter.parser import parse_config
from fw_gear_dicom_splitter.xnat_logging import stderr_log,stdout_log
from fw_gear_dicom_splitter.utils import upload_split_archive, normalize_hostname, login

def main() -> int:  # pragma: no cover
    """Parses config and run"""   
    (
        dcm_path,
        extract_localizer,
        group_by,
        max_geom_split_count,
        output_dir,
        project_id,
        session_lbl
    ) = parse_config()
    
    save_paths, success = run(
        dcm_path,
        Path(output_dir),
        group_by,
        extract_localizer,
        max_geom_splits=max_geom_split_count)

    if save_paths:
        host_clean = normalize_hostname(os.environ["XNAT_HOST"])
        login_user = os.environ["XNAT_USER"]
        login_pass = os.environ["XNAT_PASS"]

        jsessionid=login(host_clean,login_user,login_pass)

        with xnat.connect(host_clean, detect_redirect = False, jsession = jsessionid) as xnat_session:
            for archive in os.listdir("/flywheel/v0/tmp_out"):
                fp=os.path.join("/flywheel/v0/tmp_out",archive)
                if os.path.exists(fp):
                    upload_split_archive(dcm_zip_path=fp,
                                    session_label=session_lbl,
                                    project_id=project_id,
                                    xnat_session=xnat_session)

            
    elif success:
        # Splitter ran correctly, but found nothing to split
        stdout_log.info("DICOM not split, nothing to split on found.")
    else:
        stdout_log.info("DICOM not split due to error.")
        sys.exit(1)

    return 0


if __name__ == "__main__":  # pragma: no cover
    try:
        main()
    except Exception as exp:
        traceback_info = traceback.format_exc()
        stdout_log.error("There was an exception: %s Check stderr.log for more details. \n",exp)
        stderr_log.error("Exception: %s %s \n",exp,traceback_info)
        sys.exit(1)
