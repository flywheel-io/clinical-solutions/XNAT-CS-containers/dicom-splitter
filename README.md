# DICOM Splitter
This scan-level container is meant to split a scan comprised of DICOMs into two or more scans. This container corresponds to version 2.0.1 of the [DICOM Splitter](https://gitlab.com/flywheel-io/scientific-solutions/gears/splitter/-/tree/2.0.1?ref_type=tags) gear.
### Release Notes
**2.2**
- Use `Requests` package to generate JSession ID for XNATpy connection
- Disables redirects

**2.1**
- Corresponds to DICOM Splitter Gear v2.0.1
- Added checks for missing IPP or IOP
- Now attempts to split scans with secondary capture image storage (SCIS) SOPClass. In earlier versions, this container would skip any splitting if SOPClass was SCIS. If the scan contains both the **DICOM** and **secondary** resource, DICOM Splitter will not proceed.
  
**2.0**
- Corresponds to DICOM Splitter Gear v1.7.2
- Capable of multiphasic splitting via the `max_geometric_splits` config
- Change in splitting workflow to align with DICOM Splitter gear:
  | v1.0 | v2.0 |
  |------|------------------|
  | <ul><li>Try group_by splitting</ul></li> <ul><li>Try localizer splitting</ul></li> <ul><li> Try multiphasic splitting | <ul><li>Try multiphasic splitting</ul></li><ul><li>Try group_by and then localizer splitting only if multiphasic splitting did not occur</ul></li>  |

**1.0**
- Initial release corresponding to DICOM Splitter Gear v1.5.2 & an initial implementation of multiphasic splitting
---
- [DICOM Splitter](#dicom-splitter)
    - [Release Notes](#release-notes)
  - [Summary](#summary)
  - [Inputs](#inputs)
  - [Configs](#configs)
  - [Workflow](#workflow)
  - [Outputs](#outputs)

## Summary
- Use cases:
  - If there are embedded localizer/scouts within a scan
  - If the scan is multiphasic i.e, contains multiple phases in a single series. A phase refers to a sequence of slice locations with uniform spacing and direction. 
  - Split a scan by specified DICOM tag(s) (see [group_by](#configs) config).
- DICOM files should be saved either under the **DICOM** or **secondary** resource in the scan. If the scan contains both the **DICOM** and **secondary** resource, DICOM Splitter will not proceed.
- DICOM Splitter can only extract embedded localizers in MR or CT modalities. It extracts an embedded localizer through different methods, and tries to find a consensus between them:
  - Split if there is a change in neighboring frames (ImageOrientationPatient)
  - Split if there is a change in neighboring frames (ImagePositionPatient)
  - Split if there is a change in neighboring frames pixel intensity distribution
  - Split if there is a unique combination of Rows, Columns tags across the scan
- In the very unlikely event that scan is multiphasic and has a embedded localizer/scouts, DICOM Splitter needs to be run twice, see [Outputs](#outputs) for an example:
  1. Run with `max_geometric_splits` set to 0 and `extract_localizer` set to True to first separate the localizer from the primary scan DICOMs.
  2. Run on the scan containing the multiphasic DICOMs with `max_geometric_splits` set to > 1.
- The default for `group_by` config is **SeriesInstanceUID**. A scan in XNAT is always comprised of DICOMs that have the same SeriesInstanceUID. DICOM Splitter won't split the scan on SeriesInstanceUID since they are all the same, and will proceed to see if `extract_localizer` was enabled.
- Multiple tags can be provided for `group_by`:
  - For example, if `group_by` was set to `tag1,tag2` in a scan with 50 DICOMs, and if 40 of them have same value for tag1 and tag2 and 10 of them have different `tag1,tag2` combinations, DICOM Splitter will create 11 scans. Where 1 scan will have those 40 DICOMs that matched on `tag1,tag2` and the 10 remaining scans will be the 10 DICOMs that had a unique `tag1,tag2` combination.
- Split scans are saved back to the session via XNATpy with the Import Service API. Check the [Outputs](#outputs) section for more info.
- If DICOM Splitter is re-run on a scan and any splitting occurs, DICOM Splitter will save those split DICOMs as separate scans.
- Performance
  - `reserve-memory` in the command.json was set to 500 MB
  - Only 1 CPU is required

## Inputs
- **Input-scan**
    - **Description**: Input scan containing a DICOM resource with DICOM files.
    - **Type**: Scan
    - **Default**: None
## Configs
- **max_geometric_splits**
    - **Description**: Maximum number of splits to perform by image orientation and/or image position.
    - **Type**: Integer
    - **Default**: 4
    - **Note**: Set this config to an integer < 1 to skip geometric (multiphasic) splitting
- **extract_localizer**
    - **Description**: Whether or not to extract localizer from input scan. If true and the scan contains embedded localizer images, then the embedded images will be saved as their own scan.
    - **Type**: String
    - **Default**: True
- **group_by**
    - **Description**: Comma separated list of dicom tags to split the scan by.
    - **Type**: String
    - **Default**: SeriesInstanceUID
    - **Note**: To group up DICOMs that have the same Tag1 and Tag2 values, set group_by to: `Tag1,Tag2`
- **debug**
    - **Description**: Include debug level logs.
    - **Type**: Boolean
    - **Default**: False



## Workflow
- DICOM Splitter with default configs will first attempt to split the scan by phase
  - If multiphasic splitting does occur, then those outputs are saved and uploaded to the session. Multiphasic splitting is mutually exclusive with `group_by` and `extract_localizer` so they will be skipped if multiphasic splitting occurs.
  - If multiphasic splitting doesn't occur, then DICOM Splitter will attempt to split by `group_by` tags, and then extract out localizer(s).
- DICOM Splitter skips multiphasic splitting if `max_geometric_splits` is < 1.

## Outputs
DICOM Spliter will only save scans if splitting occurs. If so a variable number of scans will be created.
- The split scans that are created retain the original `SeriesNumber` and `SeriesDescription` DICOM tags but differ in `SeriesInstanceUID` and Scan ID:
  - <img src="./images/session_scans.png" width=60%/>  
  
    - Scan `2` has an embedded localizer. After running DICOM Splitter with default configs, the scan was split into localizer and non-localizer scans: `2-CT1` and `2-CT2`. In this case the split scan with the least amount of DICOMs has the localizer. The `-{modality}{numeral}` scan ID is automatically assigned by XNAT's merging process. The original `SeriesNumber` DICOM tag from scan `2` is retained in `2-CT1` and `2-CT2` DICOMs.

- In the case that a multiphasic scan has embedded localizers, DICOM Splitter is run twice:
  - <img src="./images/multiphasic_with_localizer.png" width=60%/>
    
    - DICOM Splitter was run on scan `5` with `max_geometric_splits` set to 0, and `extract_localizer` set to True, which created scans `5-CT1` and `5-CT2`. Here scan `5-CT2` is comprised of the extracted localizer DICOMs.
    - DICOM Splitter was then run with `max_geometric_splits` set to 4 on scan `5-CT1`, which was comprised of the multiphasic DICOMs. Scans `5-CT3` and `5-CT4` were then saved.