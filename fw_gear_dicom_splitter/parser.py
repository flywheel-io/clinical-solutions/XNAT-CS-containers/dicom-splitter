"""Parser module to parse gear config.json."""

import typing as t
import os
import sys
import argparse
from pathlib import Path

from .xnat_logging import stdout_log
import logging

parser=argparse.ArgumentParser()
# input and output mounts
parser.add_argument('--input_dir', type=str, required=True,default="/input")

# derived in command.json
parser.add_argument('--project_id', type=str, required=True)
parser.add_argument('--session_label', type=str, required=True)

parser.add_argument('--max_geometric_splits', type=int, required=False,default=4)
parser.add_argument('--extract_localizer', type=str, required=False,default="True")
parser.add_argument('--group_by', type=str, required=True,default="SeriesInstanceUID")
parser.add_argument('--debug', action='store_true', required=False)
args=parser.parse_args()

def parse_config() -> t.Tuple[Path, bool, t.List[str], bool]:
    """Parses gear_context config.json file.

    Returns:
        Tuple[Path, bool, t.List[str]]: tuple containing,
            - Path of dicom input
            - extract_localizer
            - group_by (unique tags to split archive on)
            - max_geom_splits
            - output_dir
    """

    if not args.debug:
        stdout_log.info("Disabling debug level logs")
        stdout_log.setLevel(logging.INFO)

    project_id = args.project_id
    session_lbl = args.session_label

    # Determine path to dicoms
    input_dir = args.input_dir
    DICOM_dir = os.path.join(input_dir, "DICOM")
    secondary_dir = os.path.join(input_dir, "secondary")
    
    if os.path.exists(DICOM_dir) and os.path.exists(secondary_dir):
        stdout_log.error("Both 'DICOM' and 'secondary' resources were found in this scan. Can't proceed!")
        sys.exit(1)
    elif os.path.exists(DICOM_dir):
        dcm_path = DICOM_dir
    elif os.path.exists(secondary_dir):
        dcm_path = secondary_dir
    else:
        stdout_log.error("Scan doesn't have 'DICOM' or 'secondary' resource. Can't proceed!")
        sys.exit(1)
    

    # if only 1 file in DICOM resource, then that is probably just an xml file, then exit
    # or if there are 0 files, then exit
    if len(os.listdir(dcm_path)) <= 1:
        stdout_log.error("There are no DICOM files under %s Exiting.",dcm_path)
        sys.exit(1)

    # Temp working directory "output"
    output_dir = os.path.join(os.environ["FLYWHEEL"],"tmp_out")

    # CONFIG
    if args.extract_localizer == "True":
        extract_localizer = True
    else:
        extract_localizer = False

    max_geom_splits = args.max_geometric_splits
    group_by = args.group_by.split(",")

    return (
        dcm_path,
        extract_localizer,
        group_by,
        max_geom_splits,
        output_dir,
        project_id,
        session_lbl)
