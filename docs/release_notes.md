# Release Notes
**2.2**
- Use `Requests` package to generate JSession ID for XNATpy connection
- Disables redirects

**2.1**
- Corresponds to DICOM Splitter Gear v2.0.1
- Added checks for missing IPP or IOP
- Now attempts to split scans with secondary capture image storage (SCIS) SOPClass. In earlier versions, this container would skip any splitting if SOPClass was SCIS. If the scan contains both the **DICOM** and **secondary** resource, DICOM Splitter will not proceed.

**2.0**
- Corresponds to DICOM Splitter Gear v1.7.2
- Capable of multiphasic splitting via the `max_geometric_splits` config
- Change in splitting workflow to align with DICOM Splitter gear:
  | v1.0 | v2.0 |
  |------|------------------|
  | <ul><li>Try group_by splitting</ul></li> <ul><li>Try localizer splitting</ul></li> <ul><li> Try multiphasic splitting | <ul><li>Try multiphasic splitting</ul></li><ul><li>Try group_by and then localizer splitting only if multiphasic splitting did not occur</ul></li>  |

**1.0**
- Initial release corresponding to DICOM Splitter Gear v1.5.2 & an initial implementation of multiphasic splitting

